/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.android.exoplayer2.demo;

import static com.google.android.exoplayer2.util.Assertions.checkNotNull;
import static weka.core.SerializationHelper.read;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.analytics.AnalyticsListener;
import com.google.android.exoplayer2.analytics.PlaybackStatsListener;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.ext.ima.ImaAdsLoader;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.DecoderInitializationException;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil.DecoderQueryException;
import com.google.android.exoplayer2.offline.DownloadRequest;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.source.LoadEventInfo;
import com.google.android.exoplayer2.source.MediaLoadData;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.MediaSourceFactory;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.ads.AdsLoader;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector.MappedTrackInfo;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.StyledPlayerControlView;
import com.google.android.exoplayer2.ui.StyledPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.DebugTextViewHelper;
import com.google.android.exoplayer2.util.ErrorMessageProvider;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.Util;
import com.opencsv.CSVWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

/** An activity that plays media using {@link SimpleExoPlayer}. */
public class PlayerActivity extends AppCompatActivity implements EventListener, MediaSourceEventListener {

  //Creating a variable for Exoplayerview
  PlayerView exoPlayerView;
  public static int track_id=5;
  public static int group_id=0;
  //Creating a variable for exoplayer
  SimpleExoPlayer exoPlayer;
  Classifier cls = null;
  //Url of video which we are loading.
  //String videoURL = "192.168.0.103/k3425.mp4";
  //  String videoURL = "http://cs1dev.ucc.ie/misl/4K_non_copyright_dataset/6_sec/x264/tearsofsteel/DASH_Files/full/tearsofsteel_enc_x264_dash.mpd";
  String videoURL = "http://192.168.0.101/KOM/8_sec/sintel_enc_x264_dash.mpd";
  int seg_index = -1;
  long prev_rebuff_dur = 0L ;
  long Target_rate =  0L ;
  HashMap<Integer, Double> map = new HashMap<>();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    int[] i = {2};
        /*
            def bitrate(id):
            if id == 1:
            return 41658758/1000
    elif id == 2:
            return 25986009/1000
    elif id == 3:
            return 15555895/1000
    elif id == 4:
            return 4374648/1000
    elif id == 5:
            return 3918050/1000
    elif id == 6:
            return 3091538/1000
    elif id == 7:
            return 2422000/1000
    elif id == 8:
            return 1800967/1000
    elif id == 9:
            return 1077559/1000
    elif id == 10:
            return 765939/1000
    elif id == 11:
            return 572129/1000
    elif id == 12:
            return 378210/1000
    elif id == 13:
            return 239148/1000
        * **/
    map.put(0,0.0);
    map.put(1,41658.758);
    map.put(2,25986.009);
    map.put(3,15555.895);
    map.put(4,4374.648);
    map.put(5,3918.050);
    map.put(6,3091.538);
    map.put(7,2422.000);
    map.put(8,1800.967);
    map.put(9,1077.559);
    map.put(10,765.939);
    map.put(11,572.129);
    map.put(12,378.210);
    map.put(13,239.148);

    setContentView(R.layout.activity_main);
    try {
      cls = null;
      cls = (Classifier) read(getAssets().open("RFCExo.model"));
      java.io.File dataPath = new java.io.File((
          getApplicationContext().getFileStreamPath("5G_8S.csv")
              .getPath()));
      CSVWriter writer = new CSVWriter(new FileWriter(dataPath,true));

      String[] clms = {"Segment_Index","Chunk_Index","Arr_Time","Del_Time","Stall_Dur","Del_Rate","Byte_Size","Buffer","Codec","Height"
          ,"Width","FPS","Chunk_Dur","ChunkStartTime","Avg_Thr","Play_Pos","Target_Rate","Rebuff_Count","Total_Stall"};
      writer.writeNext(clms);
      writer.flush();

    }catch (Exception e) {
      e.printStackTrace();
    }
    exoPlayerView = findViewById(R.id.idExoPlayerVIew);

    try {

      // Create a data source factory.
          /*  DataSource.Factory dataSourceFactory = new DefaultHttpDataSourceFactory();
            // Create a DASH media source pointing to a DASH manifest uri.
            MediaSource mediaSource =
                    new DashMediaSource.Factory(dataSourceFactory)
                            .createMediaSource(MediaItem.fromUri(Uri.parse(videoURL)));
            // Create a player instance.
            SimpleExoPlayer player = new SimpleExoPlayer.Builder(this).build();
            // Set the media source to be played.
            player.setMediaSource(mediaSource);
            // Prepare the player.
            player.prepare();**/

      // bandwisthmeter is used for
      // getting default bandwidth
      // track selector is used to navigate between
      // video using a default seekbar.
      DefaultTrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory());
      //DefaultTrackSelector trackSelector = new DefaultTrackSelector();
      trackSelector.setParameters(trackSelector.buildUponParameters().clearViewportSizeConstraints());
      // we are adding our track selector to exoplayer.
      // exoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
      exoPlayer = new SimpleExoPlayer.Builder(this).setTrackSelector(trackSelector).build();

      exoPlayer.addAnalyticsListener(new EventLogger(trackSelector));
      PlaybackStatsListener playbackStatsListener = new PlaybackStatsListener(/* keepHistory= */ true, /* callback= */ null);
      exoPlayer.addAnalyticsListener(playbackStatsListener);
      exoPlayer.addAnalyticsListener(new AnalyticsListener() {


        @Override
        public void onBandwidthEstimate(@NonNull EventTime eventTime, int totalLoadTimeMs, long totalBytesLoaded, long bitrateEstimate) {
          Target_rate = bitrateEstimate ;
        }
        @Override
        public void onLoadCompleted(@NonNull EventTime eventTime,@NonNull LoadEventInfo loadEventInfo,@NonNull MediaLoadData mediaLoadData) {
          // case.equals("uppercase") ? "JOHN" : "john";
          if(mediaLoadData.mediaStartTimeMs ==  -9223372036854775807L) return;
          if(mediaLoadData.mediaStartTimeMs ==  -9223372036854775808L) return;
          seg_index++;
          String   Chunk_Index    = mediaLoadData.trackFormat!=null ?  mediaLoadData.trackFormat.id  : "";
          Log.d("Chunk_Index",Integer.parseInt(Chunk_Index)-1 +"");
          com.google.android.exoplayer2.util.Log.d("we decided to Download=>Result",Integer.parseInt(Chunk_Index)-1+"");
          String   codec          = mediaLoadData.trackFormat!=null ? mediaLoadData.trackFormat.codecs : "";
          //String   averageBitrate = mediaLoadData.trackFormat!=null ? mediaLoadData.trackFormat.averageBitrate +"" : "";
          //String   Bitrate        = mediaLoadData.trackFormat!=null ? mediaLoadData.trackFormat.bitrate  +"" : "" ;
          String   width          = mediaLoadData.trackFormat!=null ? mediaLoadData.trackFormat.width  +""  : "";
          String   height         = mediaLoadData.trackFormat!=null ? mediaLoadData.trackFormat.height  +""  : "";
          String   FPS            = mediaLoadData.trackFormat!=null ? mediaLoadData.trackFormat.frameRate  +"" : "" ;
          String   ChunkStartTime =  mediaLoadData.mediaStartTimeMs  +""  ;
          String   Del_Time       =  loadEventInfo.loadDurationMs  +""  ;
          String   Byte_Size      =  loadEventInfo.bytesLoaded +""  ;
          String   Chunk_Dur      =  mediaLoadData.mediaEndTimeMs - mediaLoadData.mediaStartTimeMs  +"" ;
          String  Arr_Time = eventTime.realtimeMs+""; // Arrival time in milliseconds (ms)
          String  Stall_Dur = Objects.requireNonNull(playbackStatsListener.getPlaybackStats()).getTotalRebufferTimeMs()- prev_rebuff_dur + "";
          String Total_Stall = Objects.requireNonNull(playbackStatsListener.getPlaybackStats()).getTotalRebufferTimeMs()+"";
          String Rebuff_Count = Objects.requireNonNull(playbackStatsListener.getPlaybackStats()).totalRebufferCount+"";
          String   Del_Rate       = (loadEventInfo.bytesLoaded*8)/loadEventInfo.loadDurationMs + "";
          String  bandwidth_estimate = Objects.requireNonNull(playbackStatsListener.getPlaybackStats()).getMeanBandwidth()/1000+"";
          String[] entries = {seg_index+"" ,Chunk_Index, Arr_Time, Del_Time,Stall_Dur,Del_Rate,Byte_Size,exoPlayer.getBufferedPosition()+"",codec,height,width,
              FPS,Chunk_Dur,ChunkStartTime,bandwidth_estimate+"",exoPlayer.getCurrentPosition()+"",Target_rate+"",Rebuff_Count, Total_Stall};
          prev_rebuff_dur = Objects.requireNonNull(playbackStatsListener.getPlaybackStats()).getTotalRebufferTimeMs();
          // Create an empty set
          ArrayList<Attribute> attributes = new ArrayList<>();
          Bundle bundle = new Bundle();
          //Chunk_Index,Arr_Time,Del_Time,Rep_Level_1,Avg_Rep_Level_2,Del_Rate,Act_Rate_1,Avg_Act_Rate_2,Buffer,Avg_Thr,Target_Rate
          String chunk_index1 = "0" ;
          String chunk_index2 = "0" ;
          long avg_act1 = 0 ;
          long avg_act2 = 0 ;
          String tmp = "" ;
          long tmp_avgAct = 0 ;
          if(i[0] >0){
            if(i[0] ==2){ chunk_index2 = Chunk_Index ;
              avg_act2 = Long.parseLong(Del_Rate);
              i[0]-- ;}
            if(i[0] ==1){ chunk_index1 = Chunk_Index ;
              avg_act1 = Long.parseLong(Del_Rate);
              i[0]-- ;}
          }
          if(i[0] == 0) {
            tmp=chunk_index1;
            chunk_index1 = chunk_index2 ;
            chunk_index2 = tmp ;
            tmp_avgAct = avg_act1 ;
            avg_act1 = avg_act2 ;
            avg_act2 = tmp_avgAct ;
          }

          Attribute att0_Chunk_Index = new Attribute("Chunk_Index");
          Attribute att1_Arr_Time = new Attribute("Arr_Time");
          Attribute att2_Del_Time = new Attribute("Del_Time");
          Attribute att3_Rep_Level_1 = new Attribute("Rep_Level_1");
          Attribute att4_Avg_Rep_Level_2 = new Attribute("Avg_Rep_Level_2");
          Attribute att5_Del_Rate = new Attribute("Del_Rate");
          Attribute att6_Act_Rate_1 = new Attribute("Act_Rate_1");
          Attribute att7_Avg_Act_Rate_2 = new Attribute("Avg_Act_Rate_2");
          Attribute att8_Buffer = new Attribute("Buffer");
          Attribute att9_Avg_Thr = new Attribute("Avg_Thr");
          Attribute Att10_Class = new Attribute("Target_Rate");
          FastVector wekaAtt = new FastVector(10);

           //Real
          wekaAtt.addElement(att0_Chunk_Index);
          wekaAtt.addElement(att1_Arr_Time);
          wekaAtt.addElement(att2_Del_Time);
          wekaAtt.addElement(att3_Rep_Level_1);
          wekaAtt.addElement(att4_Avg_Rep_Level_2);
          wekaAtt.addElement(att5_Del_Rate);
          wekaAtt.addElement(att6_Act_Rate_1);
          wekaAtt.addElement(att7_Avg_Act_Rate_2);
          wekaAtt.addElement(att8_Buffer);
          wekaAtt.addElement(att9_Avg_Thr);
          wekaAtt.addElement(Att10_Class);

          // new instance to classify.

          Instances instances = new Instances("Rel",wekaAtt,1);
          instances.setClassIndex(9);
          Instance instance = new DenseInstance(10);
          instance.setValue(att0_Chunk_Index, Long.parseLong(Chunk_Index));
          instance.setValue(att1_Arr_Time, Long.parseLong(Arr_Time));
          instance.setValue(att2_Del_Time, Long.parseLong(Del_Time));
          //instance.setValue(att3_Rep_Level_1, map.get(Integer.parseInt(chunk_index1)));
          //instance.setValue(att4_Avg_Rep_Level_2, (map.get(Integer.parseInt(chunk_index1))+ map.get(Integer.parseInt(chunk_index2)))/2);
          instance.setValue(att3_Rep_Level_1, bandwidth_estimate);
          instance.setValue(att4_Avg_Rep_Level_2, bandwidth_estimate);
          instance.setValue(att5_Del_Rate, Long.parseLong(Del_Rate));
          instance.setValue(att6_Act_Rate_1, avg_act1);
          instance.setValue(att7_Avg_Act_Rate_2, (avg_act1+avg_act2)/2);
          instance.setValue(att8_Buffer,exoPlayer.getCurrentPosition());
          instance.setValue(att9_Avg_Thr, Long.parseLong(bandwidth_estimate));
// 1.0,278637448,2005,41658.758,23016.703,222306,55715499.0,13916114.5,159916,7152.982,1

          double prediction = 0.0 ;
          instances.add(instance);
          try {
            prediction = cls.classifyInstance(instances.instance(0));

          } catch (Exception e) {
            e.printStackTrace();
          }
          int chunkindex = (int)Math.round(prediction)-1;
          Log.d("Index Chosen Model",chunkindex+"");

          track_id = chunkindex;
          AdaptiveTrackSelection.track_id = chunkindex ;

                    /*int rendererIndex = 0;
                     MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
                     TrackGroupArray rendererTrackGroups = mappedTrackInfo == null ? null : mappedTrackInfo.getTrackGroups(rendererIndex);
                     DefaultTrackSelector.SelectionOverride selectionOverride = new DefaultTrackSelector.SelectionOverride(0, chunkindex);
                      trackSelector.setParameters(
                                 trackSelector
                                     .buildUponParameters()
                                     .setSelectionOverride(rendererIndex, rendererTrackGroups, selectionOverride));
                    Log.d("trackinfo",mappedTrackInfo.getTrackGroups(0).get(0).describeContents()+"");
                    Log.d("trackinforenderer ",trackSelector.getCurrentMappedTrackInfo().getRendererName(rendererIndex)+"");*/
          try {
            File dataPath = new File((
                getApplicationContext().getFileStreamPath("5G_8S.csv")
                    .getPath()));
            CSVWriter writer = new CSVWriter(new FileWriter(dataPath,true));

            writer.writeNext(entries);
            writer.flush();

          }catch (Exception e) {
            e.printStackTrace();
          }
        }
      });
      // we are parsing a video url
      // and parsing its video uri.
      Uri videouri = Uri.parse(videoURL);
      // we are creating a variable for datasource factory
      // and setting its user agent as 'exoplayer_view'
      DefaultHttpDataSourceFactory dataSourceFactory;
      dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
      // we are creating a variable for extractor factory
      // and setting it to default extractor factory.
      // we are creating a media source with above variables
      // and passing our event handler as null,
      // MediaSource mediaSource = new ExtractorMediaSource(videouri, dataSourceFactory, extractorsFactory, null, null);
      MediaSource mediaSource = new DashMediaSource.Factory(dataSourceFactory).createMediaSource(MediaItem.fromUri(videouri));
      // inside our exoplayer view
      // we are setting our player
      exoPlayerView.setPlayer(exoPlayer);
      //exoPlayer.setMediaItem(MediaItem.fromUri(videouri));
      exoPlayer.setMediaSource(mediaSource);
      exoPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
      exoPlayer.prepare();
      exoPlayer.play();
           /* Uri videouri = Uri.parse(videoURL);
             DataSource.Factory dataSourceFactory = new DefaultHttpDataSourceFactory();
             MediaSource mediaSource =
                    new DashMediaSource.Factory(dataSourceFactory)
                            .createMediaSource(MediaItem.fromUri(videouri));
             SimpleExoPlayer player = new SimpleExoPlayer.Builder(getApplicationContext()).build();
            exoPlayerView.setPlayer(player);
            player.setMediaSource(mediaSource);
             player.prepare();
            // we are preparing our exoplayer
            // with media source.
            //exoPlayer.prepare(mediaSource);
            // we are setting our exoplayer
            // when it is ready. ***/
    } catch (Exception e) {
      // below line is used for
      // handling our errors.
      Log.e("TAG_lab", "Error : " + e.toString());
    }

  }

}
